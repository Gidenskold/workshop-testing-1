<?php
namespace Foo\Stuff;

use Foo\Bar;

class Thing {
	public function hello() {
		echo "Hello thing";

		$bar = new Bar;
		$bar->hello();
	}
}