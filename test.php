<?php
// Ladda composers autoloader
require_once("vendor/autoload.php");

use Foo\Bar;
use Foo\Stuff\Thing;

$bar = new Bar;
$bar->hello();

$thing = new Thing;
$thing->hello();
